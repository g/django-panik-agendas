from django import forms
from django.forms.widgets import HiddenInput, RadioSelect

from .models import Booking
from .widgets import BookingLabelField, DurationField, SplitDateTimeField


class BookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = ['agenda', 'start_datetime', 'duration', 'label', 'colour', 'comment']
        field_classes = {
            'start_datetime': SplitDateTimeField,
            'duration': DurationField,
            'label': BookingLabelField,
        }
        widgets = {
            'agenda': RadioSelect,
            'colour': HiddenInput,
        }

    def __init__(self, *args, hide_agenda=False, category=None, booking=None, **kwargs):
        super().__init__(*args, **kwargs)
        if hide_agenda:
            self.fields['agenda'].widget = HiddenInput()
        elif category:
            self.fields['agenda'].queryset = self.fields['agenda'].queryset.filter(category=category)
        self.fields['label'].widget.booking = booking
        self.fields['label'].widget.category = category
