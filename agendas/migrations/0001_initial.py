# Generated by Django 1.11.29 on 2021-02-10 17:45

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('emissions', '0016_soundfile_download_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agenda',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('label', models.CharField(max_length=50, verbose_name='Label')),
                ('slug', models.SlugField()),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'Agenda',
                'verbose_name_plural': 'Agendas',
                'ordering': ['label'],
            },
        ),
        migrations.CreateModel(
            name='Booking',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('start_datetime', models.DateTimeField(verbose_name='Date/time')),
                ('duration', models.IntegerField(verbose_name='Duration')),
                ('label', models.CharField(max_length=250, verbose_name='Label')),
                ('comment', models.TextField(blank=True, verbose_name='Comment')),
                (
                    'category',
                    models.CharField(
                        blank=True,
                        choices=[
                            ('live', 'Live'),
                            ('recording', 'Recording'),
                            ('lesson', 'Lesson'),
                            ('meeting', 'Meeting'),
                            ('other', 'Other'),
                        ],
                        default='',
                        max_length=20,
                        verbose_name='Category',
                    ),
                ),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'agenda',
                    models.ForeignKey(
                        blank=False,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.Agenda',
                    ),
                ),
                (
                    'diffusion',
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to='emissions.Diffusion',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Booking',
                'verbose_name_plural': 'Bookings',
            },
        ),
    ]
