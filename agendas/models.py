import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from emissions.models import Absence, Diffusion
from nonstop.models import ScheduledDiffusion

from .app_settings import app_settings


class Category(models.Model):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['label']

    label = models.CharField(_('Label'), max_length=50)
    slug = models.SlugField()
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)
    label_presets = models.TextField(_('Presets for labels'), blank=True, null=True)

    def __str__(self):
        return self.label

    def get_label_presets(self):
        return [x for x in (self.label_presets or '').split('\n') if x]


class Agenda(models.Model):
    class Meta:
        verbose_name = _('Agenda')
        verbose_name_plural = _('Agendas')
        ordering = ['category__label', 'category_order', 'label']

    label = models.CharField(_('Label'), max_length=50)
    slug = models.SlugField()
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)
    category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.SET_NULL)
    category_order = models.PositiveIntegerField(default=0)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.label


class Booking(models.Model):
    class Meta:
        verbose_name = _('Booking')
        verbose_name_plural = _('Bookings')

    start_datetime = models.DateTimeField(_('Date/time'))
    duration = models.IntegerField(_('Duration'))
    label = models.CharField(_('Label'), max_length=250)
    colour = models.CharField(_('Colour'), max_length=20, default='#eeeeee')
    comment = models.TextField(_('Comment'), blank=True)

    agenda = models.ForeignKey(Agenda, null=True, blank=False, on_delete=models.SET_NULL)
    diffusion = models.ForeignKey('emissions.Diffusion', null=True, blank=True, on_delete=models.CASCADE)
    schedule = models.ForeignKey('emissions.Schedule', null=True, blank=True, on_delete=models.CASCADE)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.label

    def css_classes(self):
        return 'occupied'

    @property
    def emission(self):
        if self.diffusion:
            return self.diffusion.emission
        if self.schedule:
            return self.schedule.emission
        return None

    def create_secondary(self):
        if not self.emission:
            return
        try:
            pref = EmissionAgendaPreference.objects.get(emission=self.emission)
        except EmissionAgendaPreference.DoesNotExist:
            return
        if not pref.agenda:
            return
        booking, created = Booking.objects.get_or_create(
            start_datetime=self.start_datetime - datetime.timedelta(minutes=pref.pre_time or 0),
            duration=self.duration + (pref.pre_time or 0) + (pref.post_time or 0),
            label=self.label,
            comment=self.comment,
            diffusion=self.diffusion,
            schedule=self.schedule,
            agenda=pref.agenda,
        )

    def fg_colour(self):
        # luminance coefficients taken from section C-9 from
        # http://www.faqs.org/faqs/graphics/colorspace-faq/
        brightess = (
            int(self.colour[1:3], 16) * 0.212671
            + int(self.colour[3:5], 16) * 0.715160
            + int(self.colour[5:7], 16) * 0.072169
        )
        if brightess > 128:
            fg_colour = 'black'
        else:
            fg_colour = 'white'
        return fg_colour

    def end_datetime(self):
        return self.start_datetime + datetime.timedelta(minutes=self.duration)


class EmissionAgendaPreference(models.Model):
    emission = models.ForeignKey('emissions.Emission', on_delete=models.CASCADE)
    agenda = models.ForeignKey(Agenda, null=True, blank=False, on_delete=models.SET_NULL)
    pre_time = models.IntegerField(default=0)
    post_time = models.IntegerField(default=0)

    def __str__(self):
        return '%s - %s' % (self.emission, self.agenda)


@receiver(post_save, sender=Diffusion)
def save_diffusion(sender, instance, **kwargs):
    default_agenda = None
    if app_settings.DEFAULT_AGENDA_SLUG:
        default_agenda = Agenda.objects.get(slug=app_settings.DEFAULT_AGENDA_SLUG)

    bookings = Booking.objects.filter(diffusion=instance)
    if not bookings.exists():
        # remove bookings created on emission schedule
        for booking in Booking.objects.filter(start_datetime=instance.datetime, schedule__isnull=False):
            if booking.schedule.emission == instance.emission:
                booking.delete()

        booking, created = Booking.objects.get_or_create(
            diffusion=instance,
            defaults={
                'start_datetime': instance.datetime,
                'duration': instance.get_duration(),
                'label': str(instance.episode.emission),
                'agenda': default_agenda,
            },
        )
        booking.create_secondary()
    else:
        for booking in bookings:
            booking.start_datetime = instance.datetime
            booking.duration = instance.get_duration()
            booking.save()


@receiver(post_save, sender=Absence)
def save_absence(sender, instance, **kwargs):
    Booking.objects.filter(schedule__emission=instance.emission, start_datetime=instance.datetime).delete()


@receiver(post_save, sender=ScheduledDiffusion)
def save_scheduled_diffusion(sender, instance, **kwargs):
    # remove studio booking when a diffusion is programmed.
    if app_settings.DEFAULT_AGENDA_SLUG:
        default_agenda = Agenda.objects.get(slug=app_settings.DEFAULT_AGENDA_SLUG)
        Booking.objects.filter(diffusion=instance.diffusion).exclude(agenda=default_agenda).delete()
