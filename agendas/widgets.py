from django.forms.fields import CharField, SplitDateTimeField
from django.forms.widgets import SplitDateTimeWidget, TextInput


class SplitDateTimeWidget(SplitDateTimeWidget):
    template_name = 'agendas/splitdatetime.html'

    def __init__(self, *args, **kwargs):
        kwargs['time_format'] = '%H:%M'
        kwargs['date_format'] = '%Y-%m-%d'
        super().__init__(*args, **kwargs)

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['time_options'] = []
        for i in list(range(4, 24)) + list(range(0, 4)):
            context['time_options'].append('%02d:00' % i)
            context['time_options'].append('%02d:30' % i)
        return context


class SplitDateTimeField(SplitDateTimeField):
    widget = SplitDateTimeWidget


class DurationWidget(TextInput):
    template_name = 'agendas/duration.html'

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['duration_options'] = []
        for i in range(24):
            if i == 0:
                context['duration_options'].append(('30', '30 min'))
            else:
                context['duration_options'].append((str(i * 60), '%dh' % i))
                context['duration_options'].append((str(i * 60 + 30), '%dh30' % i))
        context['duration_options'].append((str(24 * 60), '24h'))
        return context


class DurationField(CharField):
    widget = DurationWidget


class BookingLabelWidget(TextInput):
    template_name = 'agendas/booking_label.html'
    booking = None

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['booking'] = self.booking
        context['category'] = self.category
        return context


class BookingLabelField(CharField):
    widget = BookingLabelWidget
