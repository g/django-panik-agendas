import datetime
import logging
import math

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from emissions.models import Emission

from .forms import BookingForm
from .models import Agenda, Booking, Category, EmissionAgendaPreference

logger = logging.getLogger('panikdb')


class Selector(ListView):
    model = Category


class Agendas(DetailView):
    model = Category
    template_name = 'agendas/agendas.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if 'date' in self.request.GET:
            today = datetime.datetime.strptime(self.request.GET['date'], '%Y-%m-%d')
        else:
            today = datetime.datetime.today()
            today = today - datetime.timedelta(days=today.weekday())
        today = today.replace(hour=0, minute=0, second=0, microsecond=0)
        # get bookings from today (4am) to next week (4am)
        bookings = Booking.objects.filter(
            start_datetime__gte=today + datetime.timedelta(hours=4),
            start_datetime__lt=today + datetime.timedelta(days=7, hours=4),
        ).order_by('start_datetime')

        class Blank:
            css_classes = 'blank'
            width = 1
            blank = True

            def __init__(self, datetime):
                self.datetime = datetime

            def attributes(self):
                return mark_safe('data-datetime="%s"' % self.datetime.strftime('%Y-%m-%d %H:%M'))

            def __str__(self):
                return ''

        category = self.get_object()
        days = []

        for i in range(7):  # a week
            day = today + datetime.timedelta(days=i)
            day_start = day + datetime.timedelta(hours=4)
            day_end = day + datetime.timedelta(days=1, hours=4)

            agenda_lines = []
            for agenda in category.agenda_set.filter(enabled=True):
                bookings_dict = {}
                for booking in bookings:
                    if booking.agenda_id != agenda.pk:
                        continue
                    if booking.start_datetime < day_start or booking.start_datetime >= day_end:
                        continue
                    if booking.start_datetime.minute == 0:
                        pass  # good
                    elif booking.start_datetime.minute > 0 and booking.start_datetime.minute < 30:
                        # make booking that would start during the first half hour
                        # start at the hour
                        booking.start_datetime = booking.start_datetime.replace(minute=0)
                    else:
                        # make booking that would start past the half hour start at the
                        # half hour
                        booking.start_datetime = booking.start_datetime.replace(minute=30)
                    bookings_dict[booking.start_datetime.time()] = booking

                cell_datetime = day.replace(hour=4)
                cell_end_datetime = cell_datetime + datetime.timedelta(hours=24)
                cells = []
                while cell_datetime < cell_end_datetime:
                    booking = bookings_dict.get(cell_datetime.time())
                    if booking:
                        booking.width = math.ceil(booking.duration / 30)
                        cells.append(booking)
                        cell_datetime = cell_datetime + datetime.timedelta(minutes=booking.width * 30)
                    else:
                        cells.append(Blank(datetime=cell_datetime))
                        cell_datetime = cell_datetime + datetime.timedelta(minutes=30)

                agenda_lines.append({'agenda': agenda, 'cells': cells})

            days.append({'day': day, 'agenda_lines': agenda_lines})

        context['day_infos'] = days
        context['hours'] = list(range(4, 24)) + list(range(0, 4))
        context['n_next_page_date'] = today + datetime.timedelta(days=28)
        context['next_page_date'] = today + datetime.timedelta(days=7)
        context['prev_page_date'] = today - datetime.timedelta(days=7)
        context['p_prev_page_date'] = today - datetime.timedelta(days=28)
        context['user_has_perm'] = self.request.user.has_perm('agendas.add_booking')

        return context


class StudioPreferences(TemplateView):
    template_name = 'agendas/studio_preferences.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emissions'] = Emission.objects.filter(archived=False).prefetch_related(
            'emissionagendapreference_set'
        )
        context['user_has_perm'] = self.request.user.has_perm('agendas.add_booking')
        return context

    def post(self, request, *args, **kwargs):
        if not self.request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        agendas_by_slug = {x.slug: x for x in Agenda.objects.filter(slug__in=['studio-1', 'studio-2'])}
        for emission in Emission.objects.filter(archived=False):
            pref, created = EmissionAgendaPreference.objects.get_or_create(emission=emission)
            current_agenda = pref.agenda
            if 'emission-%s' % emission.id in request.POST:
                pref.agenda = agendas_by_slug.get(request.POST['emission-%s' % emission.id])
            else:
                pref.agenda = None
            pre_time, post_time = pref.pre_time, pref.post_time
            pref.pre_time = int(request.POST.get('emission-%s-pre-time' % emission.id) or 0)
            pref.post_time = int(request.POST.get('emission-%s-post-time' % emission.id) or 0)
            if current_agenda != pref.agenda or pre_time != pref.pre_time or post_time != pref.post_time:
                logger.info('changed studio preferences for %s', emission.title)
                pref.save()
        return self.get(request, *args, **kwargs)


class BookingMixin:
    def get_success_url(self):
        date = self.object.start_datetime.date()
        date = date - datetime.timedelta(days=date.weekday())
        return reverse('agendas-view', kwargs={'slug': self.kwargs['cat_slug']}) + '?date=%s' % date.strftime(
            '%Y-%m-%d'
        )


class CreateBookingView(BookingMixin, CreateView):
    model = Booking
    form_class = BookingForm

    def get_initial(self):
        initial = super().get_initial()
        if 'datetime' in self.request.GET:
            initial['start_datetime'] = datetime.datetime.strptime(
                self.request.GET['datetime'],
                '%Y-%m-%d %H:%M',
            )
        if 'agenda' in self.request.GET:
            initial['agenda'] = self.request.GET['agenda']
        return initial

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['hide_agenda'] = bool('agenda' in self.request.GET)
        kwargs['category'] = Category.objects.get(slug=self.kwargs['cat_slug'])
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'agenda' in self.request.GET:
            context['agenda'] = Agenda.objects.get(pk=self.request.GET['agenda'])
        return context

    def form_valid(self, form):
        logger.info('added booking %s (%s)', form.instance, form.instance.start_datetime)
        return super().form_valid(form)


class BookingPopupView(BookingMixin, UpdateView):
    model = Booking
    form_class = BookingForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['booking'] = self.object
        kwargs['category'] = Category.objects.get(slug=self.kwargs['cat_slug'])
        return kwargs

    def post(self, request, *args, **kwargs):
        if request.POST.get('delete-button') == 'delete':
            self.object = self.get_object()
            logger.info('deleted booking %s (%s)', self.object, self.object.start_datetime)
            self.object.delete()
            return HttpResponseRedirect(self.get_success_url())
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        logger.info('modified booking %s (%s)', form.instance, form.instance.start_datetime)
        return super().form_valid(form)


class AgendasSettingsView(UpdateView):
    model = Category
    fields = ['label', 'label_presets']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lines'] = self.get_object().agenda_set.filter(enabled=True)
        return context

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def form_valid(self, form):
        logger.info('modified agendas settings %s', form.instance)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('agendas-view', kwargs={'slug': self.kwargs['slug']})


class CreateAgendaLineView(CreateView):
    model = Agenda
    fields = ['label']

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def form_valid(self, form):
        category = Category.objects.get(slug=self.kwargs['cat_slug'])
        existing_agenda = category.agenda_set.filter(label=form.instance.label).first()
        if existing_agenda:
            existing_agenda.enabled = True
            form.instance = existing_agenda
            logger.info('enabled agenda line %s', form.instance)
        else:
            form.instance.category = category
            form.instance.slug = slugify(form.instance.label)
            logger.info('added agenda line %s', form.instance)
        form.instance.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('agendas-view', kwargs={'slug': self.kwargs['cat_slug']})


class DisableAgendaLineView(DetailView):
    model = Agenda

    def get(self, request, *args, **kwargs):
        if not request.user.has_perm('agendas.add_booking'):
            raise PermissionDenied()
        agenda = self.get_object()
        agenda.enabled = False
        agenda.save()
        logger.info('disabled agenda line %s', agenda)
        messages.success(
            request,
            _(
                'This agenda line has been disabled. (you\'ll get it back if you add an agenda with the same name)'
            ),
        )
        return HttpResponseRedirect(reverse('agendas-view', kwargs={'slug': agenda.category.slug}))
