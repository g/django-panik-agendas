$(function() {
  $('table.occupancy.user-has-perm td.blank').on('click', function() {
    var agenda_pk = $(this).parents('tr').data('agenda-pk');
    var $a = $('<a></a>', {
            href: $('.actions a.new').attr('href') + '?datetime=' + $(this).data('datetime') + '&agenda=' + agenda_pk,
            rel: 'popup',
            style: 'display: none'
    });
    $a.appendTo($('#content'));
    $a.trigger('click');
  });
  $('table.studio-preferences input[type=radio]').on('change', function() {
    if ($(this).val() == 'none') {
      $(this).parents('tr').find('.around-time select').hide();
    } else {
      $(this).parents('tr').find('.around-time select').show();
    }
  });
  $('table.studio-preferences input[type=radio]:checked').trigger('change');
});
