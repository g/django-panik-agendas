from django.contrib import admin

from .models import Agenda, Booking, Category, EmissionAgendaPreference


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('label',)}


@admin.register(Agenda)
class AgendaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('label',)}


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    pass


@admin.register(EmissionAgendaPreference)
class EmissionAgendaPreferenceAdmin(admin.ModelAdmin):
    pass
