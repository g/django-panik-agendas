import datetime

from django.core.management.base import BaseCommand
from emissions.models import Absence, Diffusion, Schedule
from emissions.utils import period_program

from agendas.app_settings import app_settings
from agendas.models import Agenda, Booking


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--days', type=int, default=91, help='number of days')

    def handle(self, verbosity, **kwargs):
        date = datetime.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        nb_days = kwargs.get('days')

        default_agenda = None
        if app_settings.DEFAULT_AGENDA_SLUG:
            default_agenda = Agenda.objects.get(slug=app_settings.DEFAULT_AGENDA_SLUG)

        absences = Absence.objects.filter(
            datetime__gte=date, datetime__lt=date + datetime.timedelta(days=nb_days)
        )
        absences_dict = {x.datetime: x.emission for x in absences}

        for i in range(nb_days):
            # run a period_program() per day as it doesn't handle long periods
            for entry in period_program(
                date + datetime.timedelta(days=i),
                date + datetime.timedelta(days=i + 1),
                prefetch_sounds=False,
                prefetch_categories=False,
                include_nonstop=False,
            ):
                if not isinstance(entry, (Diffusion, Schedule)):
                    continue

                existing_bookings = Booking.objects.filter(start_datetime=entry.datetime)
                if default_agenda:
                    existing_bookings = existing_bookings.filter(agenda=default_agenda)

                absence_emission = absences_dict.get(entry.datetime)
                if absence_emission == entry.emission:
                    existing_bookings.delete()
                    continue

                if existing_bookings.exists():
                    for existing_booking in existing_bookings:
                        if existing_booking.emission != entry.emission:
                            # warn about possible conflict
                            print(
                                '%s - booking conflict between %r and %r'
                                % (entry.datetime, existing_booking, entry)
                            )
                            continue
                        if isinstance(entry, Diffusion) and not existing_booking.diffusion:
                            existing_booking.diffusion = entry
                            existing_booking.schedule = None
                            existing_booking.duration = entry.get_duration()
                            existing_booking.save()
                        elif isinstance(entry, Schedule):
                            existing_booking.duration = entry.get_duration()
                            existing_booking.save()
                    continue

                data = {
                    'start_datetime': entry.datetime,
                    'duration': entry.get_duration(),
                    'agenda': default_agenda,
                    'label': str(entry.emission),
                }

                if isinstance(entry, Schedule):
                    booking, created = Booking.objects.create(schedule=entry, **data), True
                else:
                    booking, created = Booking.objects.get_or_create(
                        diffusion=entry, agenda=default_agenda, defaults=data
                    )

                if created and not getattr(entry, 'rerun', False):
                    booking.create_secondary()
