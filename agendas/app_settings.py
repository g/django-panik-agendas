from django.conf import settings


class AppSettings:
    def get_setting(self, setting, default=None):
        return getattr(settings, 'AGENDAS_' + setting, default)

    @property
    def DEFAULT_AGENDA_SLUG(self):
        return self.get_setting('DEFAULT_AGENDA_SLUG', None)


app_settings = AppSettings()
