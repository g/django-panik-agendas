from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.Selector.as_view(), name='agendas-selector'),
    path('studio-preferences/', views.StudioPreferences.as_view(), name='agendas-studio-preferences'),
    re_path(
        r'^(?P<cat_slug>[\w,-]+)/booking/(?P<pk>\d+)/popup/$',
        views.BookingPopupView.as_view(),
        name='agendas-booking-popup',
    ),
    re_path(r'^(?P<slug>[\w,-]+)/$', views.Agendas.as_view(), name='agendas-view'),
    re_path(
        r'^(?P<cat_slug>[\w,-]+)/new/$', views.CreateBookingView.as_view(), name='agendas-create-booking'
    ),
    re_path(r'^(?P<slug>[\w,-]+)/settings/$', views.AgendasSettingsView.as_view(), name='agendas-settings'),
    re_path(
        r'^(?P<cat_slug>[\w,-]+)/settings/disable-agenda/(?P<pk>\d+)/$',
        views.DisableAgendaLineView.as_view(),
        name='agendas-disable-line',
    ),
    re_path(
        r'^(?P<cat_slug>[\w,-]+)/settings/add-agenda/$',
        views.CreateAgendaLineView.as_view(),
        name='agendas-create-line',
    ),
]
